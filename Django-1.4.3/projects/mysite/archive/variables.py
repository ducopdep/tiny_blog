from django.utils.translation import ugettext as _

def variables(request):
    return {'project_name': _('NTD Archive of PKB CV "RZD"'),
            'product_year': 2012,
            'company_name': _('PKB CV "RZD"')}