from django.contrib.auth.models import User
from django.db import models
from django.db.models.fields import IntegerField
from django.utils.translation import ugettext_lazy as _

class Divisions(models.Model):
    name = models.CharField(_('Division name'), max_length=255)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = _('Division')
        verbose_name_plural = _('Divisions')

class DocumentType(models.Model):
    name = models.CharField(_('Document type name'), max_length=100)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name = _('Document type')
        verbose_name_plural = _('Document types')

class Documents(models.Model):
    PAPERSIZE = ((0, 'A0'), (1, 'A1'), (2, 'A2'), (3, 'A3'), (4, 'A4'), (5, 'A5'), (6, 'A6'),)
    WAGONUNITS = ((0, _('Other')), (1, _('Wheelpair')), (2, _('Axle box')), (3, _('Bogie')), (4, _('Auto coupling')),
                  (5, _('Brake')), (6, _('Wage body')), (7, _('Cadre')), (8, _('Attachments')), (8, _('Wagon')),
                  (9, _('Sling van')),)
    DOCSTATE = ((0, _('Valid')), (1, _('Changed')), (2, _('Canceled')))
    title = models.CharField(_('Document title'), max_length=255)
    page_cnt = models.PositiveIntegerField(_('Page count'))
    paper_size = models.IntegerField(_('Paper size'), choices=PAPERSIZE, default=4)
    archive_number = models.CharField(_('Archive number'),
        help_text=_('Type and number of document in archive'), max_length=100)
    document_number = models.CharField(_('Document number'),
        help_text=_('Document number based on company convention'), max_length=100)
    date_approved = models.DateField(_('Date of approval'))
    approver = models.CharField(_('Document approver'),
        help_text=_('Company (and post)'), max_length=255)
    date_archived = models.DateField(_('Added to archive'),
        help_text=_('Date, when the document was added to archive'), auto_now_add=True)
    date_start = models.DateField(_('Date of commissioning'))
    division = models.ManyToManyField(Divisions, verbose_name=_('Division name'),
        help_text=_('Name of division, where this document is to be used'))
    doc_type = models.ForeignKey(DocumentType, verbose_name=_('Document type'),
        help_text=_('Instruction, draft album etc'))
    state = models.IntegerField(_('Current state'), choices=DOCSTATE, default=0)
    canceled_by = models.ForeignKey('self', verbose_name=_('Related document'),
        help_text=_('Document which replaces or modifies this one'), null=True, blank=True)
    developer = models.CharField(_('Document developer'),
        help_text=_('Company (and department if any)'), max_length=255)
    related_unit = models.IntegerField(_('Related wagon unit'), choices=WAGONUNITS, default=0)
    note = models.TextField(_('Note'), max_length=2048, null=True, blank=True) #2Kb
    def __unicode__(self):
        return self.title + ' - ' + self.document_number
    class Meta:
        verbose_name = _('Document')
        verbose_name_plural = _('Documents')

class Files(models.Model):
    doc_num = models.ForeignKey(Documents, verbose_name=_('Related document'),
        help_text=_('Document, which this file is related to'))
    file_src = models.FileField(upload_to = 'filestorage', verbose_name=_('Upload file'))
    description = models.CharField(_('File description'), max_length=255, null=True, blank=True)
    access_level = models.PositiveIntegerField(_('Required access level'))
    class Meta:
        verbose_name = _('File')
        verbose_name_plural = _('Files')
    def __unicode__(self):
        return str(self.doc_num.id) + ' - ' + self.file_src.url

class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True)

    file_access_level = models.PositiveIntegerField(_('Level of file access rights'))
    divisions_access = models.ManyToManyField(Divisions, verbose_name=_('Available divisions'),
        help_text=_('Divisions, which can be accessed by current profile'), null=True, blank=True)
    def __unicode__(self):
        return self.user.username
    class Meta:
        verbose_name = _('User profile')
        verbose_name_plural = _('User profiles')

class FileLinks(models.Model):
    user = models.ForeignKey(User)
    expires = models.DateTimeField()
    secret_link = models.CharField(max_length=11)
    file = models.ForeignKey(Files)

#class NaturalNumbers(models.IntegerField):
#    "dfnbdnbdgndx"
#    def __init__(self, verbose_name=None, min_value=None, max_value=None, **kwargs):
#        IntegerField.__init__(self, verbose_name, **kwargs)
#        self.min_value = min_value
#        self.max_value = max_value
#    def formfield(self, **kwargs):
#        return models.IntegerField.formfield(self,  min_value=self.min_value, max_value=self.max_value, **kwargs)
#    def get_internal_type(self):
#        return "IntegerField"
#
#class MMIntegerField(models.IntegerField):
#    "Min Max Integer Field - use in place of models.IntegerField"
#    def __init__(self, verbose_name=None, min_value=None, max_value=None, **kwargs):
#        IntegerField.__init__(self, verbose_name, **kwargs)
#        self.min_value = min_value
#        self.max_value = max_value
#    def formfield(self, **kwargs):
#        return models.IntegerField.formfield(self,  min_value=self.min_value, max_value=self.max_value, **kwargs)
#    def get_internal_type(self):
#        return "IntegerField"
#
#class FileLinks2(models.Model):
#    xxx = NaturalNumbers(min_value=1,max_value=5)