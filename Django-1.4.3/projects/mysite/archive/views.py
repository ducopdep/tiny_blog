import os
import random
import string
import urllib
import datetime
from urlparse import urlparse
from django.contrib.auth.decorators import login_required
from django import forms
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.encoding import smart_str
from mysite.archive.models import Documents, UserProfile, Files, FileLinks, DocumentType
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from mysite.settings import MEDIA_ROOT
import re

docs_per_page = 5 #documents displayed simultaneously
file_link_time = 15 #minutes

#django.http.urlencode but interprets None as empty string
def urlencode2(query, doseq=0):
    if hasattr(query, 'items'):
        query = query.items()
    return urllib.urlencode(
        [(smart_str(k),
          isinstance(v, (list,tuple)) and [smart_str(i or '') for i in v] or smart_str(v or ''))
        for k, v in query],
        doseq)

#def tostr(s):
#    return '' if s is None else s.encode('utf-8')

#get paginator page object based on page number string
def topage(s_page, paginator):
    if s_page.isdigit() and len(s_page)<5:
        p_page = int(s_page)
        if p_page < 1: p_page = 1
    else: p_page = 1
    try: return paginator.page(p_page) #TEMPLATE#
    # If page is not an integer, deliver first page.
    except PageNotAnInteger: return paginator.page(1)
    # If page is out of range (e.g. 9999), deliver last page of results.
    except EmptyPage: return paginator.page(paginator.num_pages)

#get user profile (create-on-get)
def get_profile(request):
    user = request.user
    try:
        profile = user.get_profile()
    except UserProfile.DoesNotExist:
        profile = UserProfile.objects.create(user=user, file_access_level='0')
    return profile

#user has access to document's divisions
def getDoc(profile, doc_num):
    try:
        doc = Documents.objects.get(id=doc_num)
        return len(profile.divisions_access.all() & doc.division.all()) > 0, doc
    except Documents.DoesNotExist:
        return False, None

@login_required
def main_page(request):
    dict = {}

    #profile
    profile = get_profile(request)
    dict['profile_divs'] = profile.divisions_access.values_list('name', flat=True)

    #filter
    docs = Documents.objects.filter(division__in=profile.divisions_access.all()).distinct()

    #form filter params
    form_filter = FormFilter(request.REQUEST)
    dict['form_filter'] = form_filter               #TEMPLATE#
    if form_filter.is_valid(): #if can install filter
        filt = form_filter.cleaned_data
        if filt['title']: docs = docs.filter(title__icontains=filt['title'])
        if filt['archive_number']: docs = docs.filter(archive_number__icontains=filt['archive_number'])
        if filt['document_number']: docs = docs.filter(document_number__icontains=filt['document_number'])
        if filt['approver']: docs = docs.filter(approver__icontains=filt['approver'])
        if filt['date_start_min']: docs = docs.filter(date_start__gte=filt['date_start_min'])
        if filt['date_start_max']: docs = docs.filter(date_start__lte=filt['date_start_max'])
        if filt['doc_type']: docs = docs.filter(doc_type=filt['doc_type'])
        if filt['state']: docs = docs.filter(state=filt['state'])
        if filt['developer']: docs = docs.filter(developer__icontains=filt['developer'])
        if filt['related_unit']: docs = docs.filter(related_unit=filt['related_unit'])
        if filt['note']: docs = docs.filter(note__icontains=filt['note'])
        dict['get_form_filter'] = urlencode2(filt)  #TEMPLATE#
    else:
        print form_filter.errors

    dict['docs_count_all'] = Documents.objects.count() #TEMPLATE#
    dict['docs_count'] = docs.count()               #TEMPLATE#

    s_page = request.GET.get('page_num', '1') if request.method == 'GET' else '1'
    paginator = Paginator(docs, docs_per_page) # Show 25 contacts per page
    dict['docs_page'] = topage(s_page, paginator)   #TEMPLATE#

    return render_to_response('tpl_main_page.html', dict,
        context_instance=RequestContext(request))

@login_required
def document_page(request, doc_num):
    profile = get_profile(request)
    dict = {'file_acc_lvl': profile.file_access_level}
    #Show Back Button if referer is another document page
    if re.match(r'^/document/(\d{1,6})$', urlparse(request.META.get('HTTP_REFERER') or '').path):
        dict['has_back_button'] = True
    #Check if document is available
    hasAccess, doc = getDoc(profile, doc_num)
    if not hasAccess:
        dict['access_error'] = True
        return render_to_response('tpl_document.html', dict,
            context_instance=RequestContext(request))

    dict['doc'] = doc
    files = doc.files_set.filter(access_level__lte=dict['file_acc_lvl'])

    now_time = datetime.datetime.now()
    #remove all expired links of ALL users
    FileLinks.objects.filter(expires__lt=now_time).delete()
    dict['files'] = []
    AlphaNumDict = string.letters + string.digits
    for file in files:
        secret_link = "".join([random.choice(AlphaNumDict) for x in range(1, 11)])
        dict['files'].append([file, secret_link, os.path.basename(str(file.file_src))])
        FileLinks(
            user=request.user,
            expires=now_time + datetime.timedelta(minutes=file_link_time),
            secret_link=secret_link,
            file=file
        ).save()

    return render_to_response('tpl_document.html', dict,
        context_instance=RequestContext(request))

@login_required
def document_file(request, file_link):
    now_time = datetime.datetime.now()
    #remove all expired links of ALL users
    FileLinks.objects.filter(expires__lt=now_time).delete()
    try:
        file_src = FileLinks.objects.get(user=request.user, secret_link=file_link).file.file_src.url
    except: #TODO: html response
        return HttpResponse('no file')
    file_path = MEDIA_ROOT + file_src
    response = HttpResponse()
    response['Content-Type'] = "application/octet-stream"
    response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_src)
    response['X-Sendfile'] = file_path
    return response

#python widget wrapper for jquery calendar
class JQueryUIDatepickerWidget(forms.DateInput):
    def __init__(self, **kwargs):
        super(forms.DateInput, self).__init__(attrs={"size":8, "class": "dateinput"}, **kwargs)
    class Media:
        css = {"all":("http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/redmond/jquery-ui.css",)}
        js = ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js",
              "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js",
              "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/i18n/jquery.ui.datepicker-ru.min.js",)

#get model field verbose name
def getVerbName(model, field):
    return model._meta.get_field(field).verbose_name

class FormFilter(forms.Form):
    title = forms.CharField(required=False, max_length=255, label=getVerbName(Documents,'title'))
    archive_number = forms.CharField(required=False, max_length=100, label=getVerbName(Documents,'archive_number'))
    document_number = forms.CharField(required=False, max_length=100, label=getVerbName(Documents,'document_number'))
    approver = forms.CharField(required=False, max_length=255, label=getVerbName(Documents,'approver'))
    date_start_min = forms.DateField(required=False, widget=JQueryUIDatepickerWidget,
        label=getVerbName(Documents,'date_start'))
    date_start_max = forms.DateField(required=False, widget=JQueryUIDatepickerWidget,
        label=getVerbName(Documents,'date_start'))
    doc_type = forms.ModelChoiceField(required=False, queryset=DocumentType.objects.all(),
        label=getVerbName(Documents,'doc_type'))
    state = forms.ChoiceField(required=False, choices=Documents.DOCSTATE, label=getVerbName(Documents,'state'))
    developer = forms.CharField(required=False, max_length=255, label=getVerbName(Documents,'developer'))
    related_unit = forms.ChoiceField(required=False, choices=Documents.WAGONUNITS,
        label=getVerbName(Documents,'related_unit'))
    note = forms.CharField(required=False, max_length=255, label=getVerbName(Documents,'note'))

    def __init__(self, *args, **kwargs):
        super(FormFilter, self).__init__(*args, **kwargs)
        if not self.fields['state'].choices[0][0] == '':
            self.fields['state'].choices.insert(0, ('','---------' ) )
        if not self.fields['related_unit'].choices[0][0] == '':
            self.fields['related_unit'].choices.insert(0, ('','---------' ) )