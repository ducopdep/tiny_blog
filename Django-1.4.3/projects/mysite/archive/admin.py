from django.contrib import admin
from mysite.archive.models import *

class DocumentsAdmin(admin.ModelAdmin):
    filter_horizontal = ('division',)

admin.site.register(Documents, DocumentsAdmin)
admin.site.register(Divisions)
admin.site.register(DocumentType)
admin.site.register(UserProfile)
admin.site.register(Files)