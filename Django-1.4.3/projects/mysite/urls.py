from django.conf.urls.defaults import patterns, include, url
from mysite.views import *
from mysite.archive.views import *
from django.contrib.auth.views import login, logout
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^mysite/', include('mysite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    (r'^accounts/login/$',  login, {'template_name': 'tpl_login.html'}),
    (r'^accounts/logout/$', logout, {'next_page': '/'}),
    (r'^$', main_page),
    (r'^document/(\d{1,6})$', document_page),
    (r'^file/([A-Za-z\d]+)$', document_file),
#    (r'^ajax_filtered_fields/', include('ajax_filtered_fields.urls')),
#    ('^hello/$', hello),
#    ('^date/$', current_datetime),
#    (r'^date/offs/(\d{1,2})/$', hours_ahead),
#    (r'^books/$', book_list),
    #(r'^auth:([a-z0-9]+)/$', get_file),
    #(r'^getfile/$', get_file),
)
urlpatterns += staticfiles_urlpatterns()


#if settings.DEBUG:
#    urlpatterns += patterns('',
#        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT + '/media'}),
#    )