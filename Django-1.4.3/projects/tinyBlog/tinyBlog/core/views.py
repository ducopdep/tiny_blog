# Create your views here.
from models import Post, Comment
from django.shortcuts import render_to_response

def index(request): #Define our function, accept a request
	posts = Post.objects.all() #ORM queries the database for all of entries.
	comments = Comment.objects.all()
	return render_to_response('index2.html', {'posts': posts, 'comments' : comments}) #Responds with passing the object items (contains info from the DB) to the template index.html

#def bootstrap(request): #Define our function, accept a request
#	return render_to_response('./static/bootstrap/css/bootstrap.css')

#def bootstrapResponsive(request): #Define our function, accept a request
#	return render_to_response('./static/bootstrap/css/bootstrap-responsive.css')

 