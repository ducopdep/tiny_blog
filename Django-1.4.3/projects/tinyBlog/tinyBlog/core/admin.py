from django.contrib import admin #Import the admin
from models import Post, Comment #Import our tinyBlog Model.
admin.site.register(Post) #Register the model with the admin
admin.site.register(Comment) #Register the model with the admin
