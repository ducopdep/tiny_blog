from django.db import models
# Create your models here.
##from tinyBlog.db import models

class  Post(models.Model):
	name = models.CharField(max_length=100, unique=True)
	description = models.TextField()
	created = models.DateTimeField()
	def __unicode__(self):
		##super( , self).__init__()
		##self.arg = arg
		return self.name

class Comment(models.Model):
	"""Structure to keep tree comments for posts
	post_id	- id of post from Posts class
	root_id - id of comment without parent, made in the root of tree
	parent_id - id of parent of comment or 0 for root comment
	level - number of steps to move comment to the right side
	date_time - date and time of comment, used to order tree
	content - text of comment

	To create tree on page - get root elements, then get children of it by
	root_id and sort them by date_time, level. Use level to move elements
	to right side, using level as coefficient of move.
	"""
	post = models.ForeignKey(Post)
	root_id = models.IntegerField()
	parent_id = models.IntegerField()
	level = models.IntegerField(max_length=11, blank=True, null=True)
	date_time = models.DateTimeField()
	content = models.TextField()
	def __unicode__(self):
		return self.content
		