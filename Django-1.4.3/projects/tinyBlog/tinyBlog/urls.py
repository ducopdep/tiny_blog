from django.conf.urls import patterns, include, url
from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	url(r'^$', 'tinyBlog.core.views.index'),
	url(r'^$', 'tinyBlog.core.views.index'),
	#url(r'^bootstrap.css$', 'tinyBlog.core.views.bootstrap'),
	#url(r'^bootstrap-responsive.css$', 'tinyBlog.core.views.bootstrapResponsive'),
	# Examples:
    # url(r'^$', 'tinyBlog.views.home', name='home'),
    # url(r'^tinyBlog/', include('tinyBlog.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
urlpatterns += staticfiles_urlpatterns()